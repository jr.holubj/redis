package com.honza.redis.redis.controller;

import com.honza.redis.redis.dto.Task;
import com.honza.redis.redis.service.DataService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/data")
@AllArgsConstructor
public class DataController {

    private DataService dataService;

    @GetMapping
    public String hello(){
        return "Hello, welcome to Redis POC app";
    }

    @GetMapping("/{id}")
    public String getValue(@PathVariable("id") final String id){
        return dataService.getValue(id);
    }


    @GetMapping("/task/{id}")
    public Task getTask(@PathVariable("id") final String id){
        return dataService.getTask(id);
    }

}
