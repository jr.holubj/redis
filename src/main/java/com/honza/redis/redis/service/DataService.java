package com.honza.redis.redis.service;

import com.honza.redis.redis.dto.Task;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Slf4j
@Service
@AllArgsConstructor
public class DataService {

    private ControlledCacheService cacheService;

    public String getValue(@NonNull final String id){
        log.info("Getting Value with id: {}", id);

        final String value = cacheService.getValueFromCache(id);
        if(Objects.nonNull(value)) {
            log.info("Value obtained from cache: {}", value);
            return value;
        } else {
            log.info("No value found in cache!");
            final String newValue = cacheService.populateCache(id);
            log.info("Populated value to cache: {}", newValue);
            return newValue;
        }
    }

    public Task getTask(final String id) {
        log.info("Getting task with id: {}", id);
        final Task task = cacheService.getTaskFromCache(id);
        if(Objects.nonNull(task)) {
            log.info("Task obtained from cache: {}", task);
            return task;
        } else {
            log.info("No task found in cache!");
            final Task newTask = new Task(id, "Store task in redis", 13);
            cacheService.populateTaskToCache(newTask.getId(), newTask);
            log.info("Populated task to cache: {}", newTask);
            return newTask;
        }
    }
}
