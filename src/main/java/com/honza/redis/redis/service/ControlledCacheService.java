package com.honza.redis.redis.service;

import com.honza.redis.redis.dto.Task;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.TimeToLive;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ControlledCacheService {

    public static final String MY_CONTROLLED_CACHE = "myControlledCache";
    public static final String MY_CONTROLLED_TASK_CACHE = "myControlledTaskCache";
    private static final String MY_PREFIX = "dataPrefix_";

    public static String getCacheKey(@NonNull final String relevant){
        return MY_PREFIX + relevant;
    }

    @Cacheable(cacheNames = MY_CONTROLLED_CACHE, key = "T(com.honza.redis.redis.service.ControlledCacheService).getCacheKey(#id)")
    public String getValueFromCache(@NonNull final String id) {
        return null;
    }

    @CachePut(cacheNames = MY_CONTROLLED_CACHE, key = "T(com.honza.redis.redis.service.ControlledCacheService).getCacheKey(#id)")
    public String populateCache(@NonNull final String id) {
        return "This is value: " + id;
    }

    @CacheEvict(cacheNames = MY_CONTROLLED_CACHE)
    public void removeFromCache(){

    }

    @Cacheable(cacheNames = MY_CONTROLLED_TASK_CACHE, key = "T(com.honza.redis.redis.service.ControlledCacheService).getCacheKey(#id)")
    public Task getTaskFromCache(final String id) {
        return null;
    }

    @CachePut(cacheNames = MY_CONTROLLED_TASK_CACHE, key = "T(com.honza.redis.redis.service.ControlledCacheService).getCacheKey(#id)")
    public Task populateTaskToCache(final String id, final Task task) {
        return task;
    }
}
