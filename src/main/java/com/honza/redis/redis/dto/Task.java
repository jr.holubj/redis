package com.honza.redis.redis.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@AllArgsConstructor
@Data
public class Task implements Serializable {
    private static final long serialversionUID = 129348938L;

    private final String id;
    private final String name;
    private final Integer priority;
}
