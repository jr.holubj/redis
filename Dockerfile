FROM openjdk:8
EXPOSE 8080
ADD target/spring-boot-redis.jar spring-boot-redis.jar
ENTRYPOINT ["java", "-jar", "/spring-boot-redis.jar"]